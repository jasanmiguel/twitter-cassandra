# Twitter Cassandra

> Practica Sistemas distribuidos UEM 2015

## Descripcion

Para la prueba de Apache Spark Streaming y su integracion con Cassandra se ha implementado un capturador de twitter basado en un conjunto de palabras clave que permitan capturar el stream de twitter y almacenarlo para su posterior analisis.

Para ponerle un contexto, se ha definido un conjunto de palabras clave relacionadas con Juego de Tronos, de manera q el analisis del resultado pueda servir como una aproximacion a un analisis de audiencia del capitulo numero 4 de la quinta temporada (Sons of the Harpy)

6h antes de su emision en USA () y hasta su emision en España (24h despues) se estaran capturando tweets y almacenando en Cassandra.

Posteriormente se procedera a su analisis para obtener el hashtag mas mencionado del capitulo, la localizacion frecuente de los usuarios y un geoposicionamiento de los tweets que contengan informacion para ser posicionados en un mapa.

Para el geoposicionamiento, utilizaremos CartoDb, de manera que se podran probar sus diferentes opciones de visualizacion.

## Software, librerias y versiones

Para la correcta ejecucion de la practica es necesario

 - Apache Spark 1.3.1 
 - Apache Cassandra 2.0.11
 - Spark cassandra Connector 1.3.0
 - Twitter4j 3.0.6
 - Spark Streaming Twitter 1.1.0 (cdh5.2.2)

Todas las librerias se encuentran en la carpeta /lib del repositorio

## Instalacion y ejecucion

Exportar las variables de entorno para escribir en S3

        export AWS_SECRET_ACCESS_KEY={secret-acces-key}
        export AWS_ACCESS_KEY_ID={access-key}

Instalacion desde el repositorio

        git clone git@bitbucket.org:jasanmiguel/twitter-cassandra.git

Empaquetar el jar

        cd twitter-cassandra
        sbt package

## Ejecucion

### Extrayendo tweets...

	$HOME/spark/bin/spark-submit --class "TwitterCassandraApp" --master local[4] --jars $HOME/twitter-cassandra/lib/spark-streaming-twitter_2.10-1.1.0-cdh5.2.2.jar,$HOME/twitter-cassandra/lib/twitter4j-core-3.0.6.jar,$HOME/twitter-cassandra/lib/twitter4j-stream-3.0.6.jar,$HOME/twitter-cassandra/lib/spark-cassandra-connector-assembly-1.3.0-SNAPSHOT.jar $HOME/twitter-cassandra/target/scala-2.10/twitter-cassandra_2.10-1.0.jar --write

### Leyendo y analizando tweets...

	$HOME/spark/bin/spark-submit --class "TwitterCassandraApp" --master local[4] --jars $HOME/twitter-cassandra/lib/spark-streaming-twitter_2.10-1.1.0-cdh5.2.2.jar,$HOME/twitter-cassandra/lib/twitter4j-core-3.0.6.jar,$HOME/twitter-cassandra/lib/twitter4j-stream-3.0.6.jar,$HOME/twitter-cassandra/lib/spark-cassandra-connector-assembly-1.3.0-SNAPSHOT.jar $HOME/twitter-cassandra/target/scala-2.10/twitter-cassandra_2.10-1.0.jar --read

## Presentacion

[Descargar]()