import org.apache.spark.SparkContext
import org.apache.spark.SparkContext._
import org.apache.spark.SparkConf

import com.datastax.spark.connector._
import com.datastax.spark.connector.cql.CassandraConnector

import org.apache.spark.streaming._
import org.apache.spark.streaming.twitter._
import org.apache.spark.streaming.StreamingContext._

object CassandraTest
{
    def main(args: Array[String])
    {
        //TODO
    }

    def saving()
    {
        System.setProperty("twitter4j.oauth.consumerKey", "QTDiJtx19VzL87YTrj136IXWD")
        System.setProperty("twitter4j.oauth.consumerSecret", "tplI4DMLvWtxz4HollwnyF1HsiqC9Y0L07bLfxugXXHnJHD4KP")
        System.setProperty("twitter4j.oauth.accessToken", "4861891-x0Wo1ktui5yQKYAgiTh0n1Jce4M12ilNJvvA8ORCWd")
        System.setProperty("twitter4j.oauth.accessTokenSecret","D608uspUCHDX1npapzq6isw05eVeEcCLEyWi7Od7Xmky8")

        val conf = new SparkConf(true).setAppName("Cassandra Test").set("spark.cassandra.connection.host", "163.117.148.120")

        val sc = new SparkContext(conf)
        var ssc = new StreamingContext(sc, Seconds(1))
        var tweets = TwitterUtils.createStream(ssc, None)

        val KEYSPACE = "twitterjose"
        val TABLE = "tweets"

        CassandraConnector(conf).withSessionDo { session =>
          session.execute(s"CREATE KEYSPACE IF NOT EXISTS $KEYSPACE WITH REPLICATION = {'class': 'SimpleStrategy', 'replication_factor': 1 }")
          session.execute(s"CREATE TABLE IF NOT EXISTS $KEYSPACE.$TABLE (userid text,message text,hashtags text,date text,tweetlocation text,userlocation text, PRIMARY KEY(userid,date)) WITH CLUSTERING ORDER BY (date DESC)")
          session.execute(s"TRUNCATE $KEYSPACE.$TABLE")
        }

        case class tweetClass(userid:String, message: String, hashtags: String, date: String, tweetlocation: String, userlocation: String)

        val dstream = tweets.filter(_.getLang == "es").map(tweet => {
            tweetClass(
              tweet.getUser.getId().toString,
              tweet.getText(),
              tweet.getHashtagEntities().map(_.getText()).mkString(","),
              tweet.getCreatedAt.toString,
              tweet.getGeoLocation() match {
                case null => null
                case loc => loc.getLatitude().toString + "," + loc.getLongitude().toString
              },
              tweet.getUser.getLocation()
            )
          })

        dstream.count.print
        dstream.foreachRDD(rdd => rdd.saveToCassandra(KEYSPACE, TABLE, SomeColumns("userid","message","hashtags","date","tweetlocation","userlocation")))

        ssc.start()
        ssc.awaitTermination()

    }

    def reading()
    {
        val conf = new SparkConf(true).setAppName("Cassandra Test").set("spark.cassandra.connection.host", "163.117.148.120")
        val sc = new SparkContext(conf)
        
        val KEYSPACE = "twitterjose"
        val TABLE = "tweets"
        
        val rdd = sc.cassandraTable(KEYSPACE, TABLE)
        println(rdd.count)
        println(rdd.first)
        rdd.toArray.foreach(println)
    }

}