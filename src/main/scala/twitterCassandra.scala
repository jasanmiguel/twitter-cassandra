import org.apache.spark.SparkContext
import org.apache.spark.SparkContext._
import org.apache.spark.SparkConf

import com.datastax.spark.connector._
import com.datastax.spark.connector.cql.CassandraConnector

import org.apache.spark.streaming._
import org.apache.spark.streaming.twitter._
import org.apache.spark.streaming.StreamingContext._

import java.text.SimpleDateFormat
import java.util.Calendar

case class tweetClass(userid:String, 
                      message: String, 
                      hashtags: String, 
                      date: String, 
                      tweetlocation: String, 
                      userlocation: String, 
                      mediaurl:String)

class TwitterCassandra()
{
    val CASSANDRA_IP = "163.117.148.120"
    val KEYSPACE = "twitterjose"
    val TABLE = "gameofthrones_506"

    val conf = new SparkConf(true).setAppName("Twitter Cassandra")
                                  .set("spark.cassandra.connection.host", CASSANDRA_IP)
                                  .set("fs.s3.impl", "org.apache.hadoop.fs.s3native.NativeS3FileSystem")
                                  .set("fs.s3.awsAccessKeyId","AKIAIW53A65TGR4G6ZSQ")
                                  .set("fs.s3.awsSecretAccessKey","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")
        
    val sc = new SparkContext(conf)

    def setup()
    {
        CassandraConnector(conf).withSessionDo { session =>
          session.execute(s"CREATE KEYSPACE IF NOT EXISTS $KEYSPACE WITH REPLICATION = {'class': 'SimpleStrategy', 'replication_factor': 1 }")
          session.execute(s"CREATE TABLE IF NOT EXISTS $KEYSPACE.$TABLE (userid text, message text, hashtags text, date text, tweetlocation text, userlocation text, mediaurl text, PRIMARY KEY(date,userid)) WITH CLUSTERING ORDER BY (userid DESC)")
          /* session.execute(s"TRUNCATE $KEYSPACE.$TABLE") */
        }
    }

    def write()
    {
        var filters = Array("#GoTSeason5", 
		                    "#GoT", 
		                    "#GameofThrones",
		                    "Game of thrones", 
		                    "Daenerys","Targeryen", 
		                    "#MotherOfDragons",
		                    "Margaery","Tyrell",
		                    "Tyrion", "Lannister", "Varys",
		                    "#TheNorthRemembers",
		                    "Sansa", "Littlefinger", "Bolton", "Ramsey",
		                    "#TeamTyrell",
		                    "#TeamLannister",
		                    "#TeamStark",
		                    "#SonsOfTheArpy",
		                    "Sons of the Arpy",
		                    "#VivePoniente", "Juego de Tronos", 
		                    "Hijos de la arpia", "#JuegodeTronos"
		                    )

        System.setProperty("twitter4j.oauth.consumerKey", "QTDiJtx19VzL87YTrj136IXWD")
        System.setProperty("twitter4j.oauth.consumerSecret", "tplI4DMLvWtxz4HollwnyF1HsiqC9Y0L07bLfxugXXHnJHD4KP")
        System.setProperty("twitter4j.oauth.accessToken", "4861891-x0Wo1ktui5yQKYAgiTh0n1Jce4M12ilNJvvA8ORCWd")
        System.setProperty("twitter4j.oauth.accessTokenSecret","D608uspUCHDX1npapzq6isw05eVeEcCLEyWi7Od7Xmky8")

        var ssc = new StreamingContext(sc, Seconds(1))
        var tweets = TwitterUtils.createStream(ssc, None, filters)

        val dstream = tweets.map(tweet => {
            tweetClass(
              tweet.getUser.getId().toString,
              tweet.getText(),
              tweet.getHashtagEntities().map(_.getText()).mkString(","),
              tweet.getCreatedAt.toString,
              tweet.getGeoLocation() match {
                case null => null
                case loc => loc.getLatitude().toString + "," + loc.getLongitude().toString
              },
              tweet.getUser.getLocation(),
              tweet.getMediaEntities().map(_.getMediaURL()).mkString(",")
            )
          })

        dstream.count.print
        dstream.foreachRDD(rdd => rdd.saveToCassandra(KEYSPACE, TABLE, SomeColumns("userid","message","hashtags","date","tweetlocation","userlocation","mediaurl")))

        ssc.start()
        ssc.awaitTermination()
    }

    def read()
    {
        val conection = sc.cassandraTable[String](KEYSPACE, TABLE)

        println("\nThe most mencioned hashtags from the episode")
        println("--------------------------------------------")
        val hastagsDB = conection.select("hashtags")
        val hastags = hastagsDB.flatMap(_.split(",")).map(x => (x.trim.toLowerCase(),1)).reduceByKey(_+_).map(_.swap)
        hastags.sortByKey(false).take(50).foreach(println)

        println("\nCounting words")
        println("--------------------------------------------")
        val messagesDB = conection.select("message")
        val messages = messagesDB.flatMap(_.split(" ")).map(x => (x.trim,1)).reduceByKey(_+_).map(_.swap)
        messages.sortByKey(false).take(50).foreach(println)

        println("\nCounting user locations")
        println("--------------------------------------------")
        val userlocationDB = conection.select("userlocation")
        val locations = userlocationDB.flatMap(_.split(",")).map(x => (x.trim.toLowerCase(),1)).reduceByKey(_+_).map(_.swap)
        locations.sortByKey(false).take(50).foreach(println)

        println("\nNumber of tweets for hour")
        println("--------------------------------------------")
        val datesDB = conection.select("date")
        var tweetsByHour = datesDB.map(date => {
            val dateParser = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzzz yyyy").parse(date)
            val cal = Calendar.getInstance()
            cal.setTime(dateParser)
            cal.add(Calendar.HOUR_OF_DAY,2) //To adapt GMT+2
            val newDate = cal.getTime();
            val hour = newDate.getDay().toString+"-"+newDate.getHours().toString+":00"
            hour
        }).map(x => (x,1)).reduceByKey(_+_).map(_.swap)
        tweetsByHour.sortByKey(false).foreach(println)

        println("\nExtracting data to import into CartoDb")
        println("--------------------------------------------")

        val timestamp = System.currentTimeMillis / 1000
        val cartoDbFile = "s3n://bigdata-uem/jose/twitter-cassandra/"+TABLE+"_"+timestamp+".csv"
        val allDB = sc.cassandraTable[(String,String,String,String)](KEYSPACE, TABLE).select("message","date","userlocation","tweetlocation")
        val csvResults = allDB.map { case (message, date, userlocation, tweetlocation) => {
            var latitude = "0"
            var longitude = "0"    
            if(tweetlocation != null)
            {
                val location = tweetlocation.split(",")
                if(!location.isEmpty){
                    latitude = location(0)
                    longitude = location(1)
                }
            }
            val timestamp = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzzz yyyy").parse(date).getTime()/1000
            Array(message, timestamp, userlocation, latitude, longitude)
          }.mkString(";") }
        csvResults.saveAsTextFile(cartoDbFile)
        println("--> File located at: "+cartoDbFile)
    }

}

object TwitterCassandraApp
{
    def main(args: Array[String])
    {
        if (args.length < 1) 
        {
            System.err.println("\nUsage: TwitterCassandraApp --write or TwitterCassandraApp --read\n")
            System.exit(1)
        }

        println("\nStart running the program") 
        val app = new TwitterCassandra()

        if(args(0) == "--write")
        {
            println("\nCreating the key space and the tables")
            app.setup();

            println("\nReading from twitter and saving into cassandra...")
            app.write();
        } 
        else if(args(0) == "--read")
        {
            println("\nReading from cassandra and analyzing tweets...")
            app.read();
        }

        println("\nSuccessfully completed running the program\n\n")
    }
}
